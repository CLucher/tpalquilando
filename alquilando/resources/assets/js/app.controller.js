(function() {
    'use strict'

    angular
        .module('app')
        .controller('AppController', AppController)

    AppController.$inject = ['languageService']

    function AppController(languageService) {
        var vm = this

        init()

        vm.selectBranch = function(branch) {
            vm.branch = branch
            sessionStorage.setItem('branch', JSON.stringify(branch))
        }

        vm.selectFamily = function(family) {
            vm.family = family
            sessionStorage.setItem('family', JSON.stringify(family))
        }

        vm.selectLanguage = function(lang) {
            vm.lang = lang
            vm.data = languageService.data[lang]
        }

        function init() {
            vm.branch = JSON.parse(sessionStorage.getItem('branch'))
            vm.family = JSON.parse(sessionStorage.getItem('family'))
            vm.data = languageService.data.es
            vm.lang = 'es'
        }
    }
})();
